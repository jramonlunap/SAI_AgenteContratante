<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html ng-app="appAgentes">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<%=getServletContext().getContextPath()%>/css/tables-css.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.min.js"></script>
        <script src="js/CtrlAgente.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"/>
        <title>Agentes</title>
    </head>
    <body>
        <%String id =  request.getParameter("id");%>
        <h1>Editar Agentes</h1>
        <div ng-controller="CtrlAgente">
 
           <input type="hidden" name="id" value="<%= id %>">
           <label>Clave </label></br><input type="text" name="clave" ng-bind="{{a.clave}}"></br>
            <label>Nombre </label></br><input type="text" name="nombre" value="${articulo.nombre}"></br>
            <input type="submit" value="Guardar"/>
        </div>
    </body>
</html>
