var appAgentes = angular.module('appAgentes', []);

appAgentes.controller('CtrlAgente', ['$scope', '$http', '$log',
    function ($scope, $http, $log) {
        $log.debug('Definiendo controlador...');
        $scope.listaAgentes = [];
        $scope.listaLicitaciones = [];
        $scope.cargarAgentes = function () {
            //Toma el valor de tareas y concatenada otra funcion que ocurre si la primera es verdadera
            $http.get('agente')
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        //NO ES UTIL: $log.debug("Datos: " + respuesta.data);
                        $scope.listaAgentes = respuesta.data;
                    });
        };
        
        $scope.cargarLicitaciones = function () {
            //Toma el valor de tareas y concatenada otra funcion que ocurre si la primera es verdadera
            $http.get('licitaciones')
                    .then(function (respuesta) {
                        $log.debug(respuesta.data);
                        //NO ES UTIL: $log.debug("Datos: " + respuesta.data);
                        $scope.listaLicitaciones = respuesta.data;
                    });
        };


        $scope.nuevoAgente = {};
        $scope.nuevoAgente.idAgente = {};
        
        $scope.editarPagina = function (id){
            $scope.listaAgentes = id.a.id;
          location.href = "/SAI/formularioEditar.jsp?id="+id.a.id;  
        };
        
        $scope.borrarAgente = function (id) {
            $http.delete('agente/'+id.a.id).then(function (respuesta) {
                $log.debug(respuesta.data);
            });
            location.reload();
        };
        
        $scope.guardarAgente = function () {
            $scope.nuevoAgente.idAgente = {};
            $scope.nuevoAgente.idAgente.id = 1;
            $http.post('agentes', $scope.nuevoAgente)
                    .then(function (respuesta) {
                        $scope.nuevoAgente = {};
                        $scope.cargarAgentes();
                        location.href = "/SAI/lista.jsp";
                    }, function (respuesta) {
                        $log.debug("POST ERROR");
                        location.href = "/SAI/lista.jsp";
                    });
        };

        $scope.editarAgente = function () {
            $log.debug("editarArticulo");
            $scope.nuevoAgente.idAgente = {};
            $scope.nuevoAgente.idAgente.id = 1;
            $http.put('agentes', $scope.nuevoAgente)
                    .then(function (respuesta) {
                        $log.debug("PUT Exito");
                        $scope.nuevoAgente = {};
                        $scope.cargarAgentes();
                        location.href = "/SAI/lista.jsp";
                    }, function (respuesta) {
                        $log.debug("POST ERROR");
                        location.href = "/SAI/lista.jsp";
                    });
        };

        $scope.cargarAgentes();
        $scope.cargarLicitaciones();
        //$scope.cargarArticulosPorID(130);
        $log.debug("Controlador definido");
    }]);