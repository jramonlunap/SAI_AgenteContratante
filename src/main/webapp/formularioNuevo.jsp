<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html ng-app="appAgentes">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<%=getServletContext().getContextPath()%>/css/tables-css.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.min.js"></script>
        <script src="js/CtrlAgente.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"/>
        <title>Licitaciones</title>
    </head>

  

    <body>
        <h1>Nueva Licitacion</h1>
        <div id="forma" ng-controller="CtrlAgente">
                <label>ID </label></br><input type="number" ng-model="nuevoAgente.id"></br>
                <label>Clave </label></br><input type="text" ng-model="nuevoAgente.clave"></br>
                <label>Nombre </label></br><input type="text" ng-model="nuevoAgente.nombre"></br>
                <label>Fecha de Alta </label></br><input type="date" ng-model="nuevoAgente.fechaAlta"></br>
                <label>Catálogo del Agente</label></br>
                <select ng-model="nuevoAgente.idAgente" 
                        ng-options="c.id as c.nombre for c in listaCatalogos"></select>
                <input type="button" value="Guardar" ng-click="guardarAgente()"/>
        </div>
    </body>
</html>
