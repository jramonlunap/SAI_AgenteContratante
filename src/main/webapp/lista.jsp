<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html ng-app="appAgentes">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<%=getServletContext().getContextPath()%>/css/tables-css.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.6/angular.min.js"></script>
        <script src="js/CtrlAgente.js"></script>        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Agentes</title>
    </head>
    <body>
        <h1>Lista de Agentes</h1>
        <div>
            <label><i>Nuevo Agente</i></label>
            <a href="/SAI/formularioNuevo.jsp"><i class="material-icons button edit">&#xE03C;</a></i></br>
    </div>
        <div ng-controller="CtrlAgente">
        <table>
            <thead>
                <tr>
                    <th colspan="24">Agentes</th>
                </tr>            
                <tr>
                    <th colspan="3">ID</th>
                    <th colspan="3">Clave</th>
                    <th colspan="3">Nombre</th>
                    <th colspan="3">Precio de Compra</th>
                    <th colspan="3">Precio de Venta</th>
                    <th colspan="3">Fecha de Alta</th>
                    <th colspan="3">Fecha de Venta</th>
                    <th>  </th>
                </tr>
            </thead>
            <tbody>                

                <tr ng-repeat="a in listaArticulos">
                        <td colspan="3">{{a.id}}</td>
                        <td colspan="3">{{a.clave}}</td>
                        <td colspan="3">{{a.nombre}}</td>
                        <td colspan="3">{{a.precioCompra}} pesos</td>
                        <td colspan="3">{{a.precioVenta}} pesos</td>
                        <td colspan="3">{{a.fechaAlta| date: 'dd/MM/yyyy'}} </td>
                        <td colspan="3">{{a.fechaVenta| date: 'dd/MM/yyyy'}} </td>
                        <td>
                            <a id="{{a.id}}" ng-click="editarPagina(this)"><i class="material-icons button edit">edit</a></i>
                            <a id="{{a.id}}" ng-click="borrarArticulo(this)"><i class="material-icons button delete">delete</a></i>

                        </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>
