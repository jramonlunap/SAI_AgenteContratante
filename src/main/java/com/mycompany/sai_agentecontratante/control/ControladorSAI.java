/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.control;

import com.mycompany.sai_agentecontratante.entidades.Agentecontratante;
import com.mycompany.sai_agentecontratante.entidades.Licitacion;
import com.mycompany.sai_agentecontratante.entidades.Proveedor;
import com.mycompany.sai_agentecontratante.entidades.UsuarioSai;
import com.mycompany.sai_agentecontratante.oad.OadAgenteContratante;
import com.mycompany.sai_agentecontratante.oad.OadLicitacion;
import com.mycompany.sai_agentecontratante.oad.OadProveedor;
import com.mycompany.sai_agentecontratante.oad.OadUsuarios;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author darkm
 */
public class ControladorSAI {

    @Autowired
    OadUsuarios oadUsuarios;

    @GetMapping("/usuarios/{id}")
    public UsuarioSai getUsuarios(@PathVariable("id") Integer idUsuario) {
        return oadUsuarios.getPorId(idUsuario);
    }

    @Autowired
    OadLicitacion oadLicitacion;

    @GetMapping("/licitacion")
    public List<Licitacion> getLicitaciones() {
        return oadLicitacion.findAll();
    }

    @GetMapping("/licitacion/{id}")
    public Licitacion getLicitacionesPorId(@PathVariable("id") Integer idLicitacion) {
        return oadLicitacion.getPorId(idLicitacion);
    }

    @Autowired
    OadProveedor oadProveedor;

    @GetMapping(value = "/proveedor", params = {"nombre"})
    public List<Proveedor> buscarProveedorPorNombre(@RequestParam("nombre") String nombre) {
        return oadProveedor.buscarPorNombre(nombre);
    }

    @Autowired
    OadAgenteContratante oadAgenteContratante;

    @GetMapping("/agenteContratante/{id}")
    public Agentecontratante getAgentePorId(@PathVariable("id") Integer idAgente) {
        return oadAgenteContratante.getPorId(idAgente);
    }
}
