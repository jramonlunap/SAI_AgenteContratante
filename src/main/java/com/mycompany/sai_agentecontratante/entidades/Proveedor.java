/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author darkm
 */
@Entity
@Table(name = "PROVEEDOR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proveedor.findAll", query = "SELECT p FROM Proveedor p")
    , @NamedQuery(name = "Proveedor.findByClaveproveedor", query = "SELECT p FROM Proveedor p WHERE p.claveproveedor = :claveproveedor")})
public class Proveedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLAVEPROVEEDOR")
    private Integer claveproveedor;

    public Proveedor() {
    }

    public Proveedor(Integer claveproveedor) {
        this.claveproveedor = claveproveedor;
    }

    public Integer getClaveproveedor() {
        return claveproveedor;
    }

    public void setClaveproveedor(Integer claveproveedor) {
        this.claveproveedor = claveproveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claveproveedor != null ? claveproveedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedor)) {
            return false;
        }
        Proveedor other = (Proveedor) object;
        if ((this.claveproveedor == null && other.claveproveedor != null) || (this.claveproveedor != null && !this.claveproveedor.equals(other.claveproveedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.sai_agentecontratante.entidades.Proveedor[ claveproveedor=" + claveproveedor + " ]";
    }
    
}
