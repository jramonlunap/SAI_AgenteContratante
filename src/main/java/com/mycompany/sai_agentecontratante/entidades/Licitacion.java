/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author darkm
 */
@Entity
@Table(name = "LICITACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Licitacion.findAll", query = "SELECT l FROM Licitacion l")
    , @NamedQuery(name = "Licitacion.findByAnexoSolicitudPresupuesto", query = "SELECT l FROM Licitacion l WHERE l.anexoSolicitudPresupuesto = :anexoSolicitudPresupuesto")
    , @NamedQuery(name = "Licitacion.findByCaracterLicitacion", query = "SELECT l FROM Licitacion l WHERE l.caracterLicitacion = :caracterLicitacion")
    , @NamedQuery(name = "Licitacion.findByClaveGanador", query = "SELECT l FROM Licitacion l WHERE l.claveGanador = :claveGanador")
    , @NamedQuery(name = "Licitacion.findByDescripcionCorta", query = "SELECT l FROM Licitacion l WHERE l.descripcionCorta = :descripcionCorta")
    , @NamedQuery(name = "Licitacion.findByDescripcionDetallada", query = "SELECT l FROM Licitacion l WHERE l.descripcionDetallada = :descripcionDetallada")
    , @NamedQuery(name = "Licitacion.findByDocumentosAnexo", query = "SELECT l FROM Licitacion l WHERE l.documentosAnexo = :documentosAnexo")
    , @NamedQuery(name = "Licitacion.findByEmpresaAgenteAsignado", query = "SELECT l FROM Licitacion l WHERE l.empresaAgenteAsignado = :empresaAgenteAsignado")
    , @NamedQuery(name = "Licitacion.findByFechaAdjudicacion", query = "SELECT l FROM Licitacion l WHERE l.fechaAdjudicacion = :fechaAdjudicacion")
    , @NamedQuery(name = "Licitacion.findByFechaAperturaSesionPreguntas", query = "SELECT l FROM Licitacion l WHERE l.fechaAperturaSesionPreguntas = :fechaAperturaSesionPreguntas")
    , @NamedQuery(name = "Licitacion.findByFechaCierreRecepcionPropuestas", query = "SELECT l FROM Licitacion l WHERE l.fechaCierreRecepcionPropuestas = :fechaCierreRecepcionPropuestas")
    , @NamedQuery(name = "Licitacion.findByFechaCierreSesionPreguntas", query = "SELECT l FROM Licitacion l WHERE l.fechaCierreSesionPreguntas = :fechaCierreSesionPreguntas")
    , @NamedQuery(name = "Licitacion.findByFechaCierreSesionRespuestas", query = "SELECT l FROM Licitacion l WHERE l.fechaCierreSesionRespuestas = :fechaCierreSesionRespuestas")
    , @NamedQuery(name = "Licitacion.findByFechaCreacion", query = "SELECT l FROM Licitacion l WHERE l.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "Licitacion.findByFechaPublicacion", query = "SELECT l FROM Licitacion l WHERE l.fechaPublicacion = :fechaPublicacion")
    , @NamedQuery(name = "Licitacion.findByFechaRecepcionPropuestas", query = "SELECT l FROM Licitacion l WHERE l.fechaRecepcionPropuestas = :fechaRecepcionPropuestas")
    , @NamedQuery(name = "Licitacion.findByFechaSesionRespuestas", query = "SELECT l FROM Licitacion l WHERE l.fechaSesionRespuestas = :fechaSesionRespuestas")
    , @NamedQuery(name = "Licitacion.findByFolioLicitacion", query = "SELECT l FROM Licitacion l WHERE l.folioLicitacion = :folioLicitacion")
    , @NamedQuery(name = "Licitacion.findByFolioPresupuestoAprobado", query = "SELECT l FROM Licitacion l WHERE l.folioPresupuestoAprobado = :folioPresupuestoAprobado")
    , @NamedQuery(name = "Licitacion.findByIdLicitacion", query = "SELECT l FROM Licitacion l WHERE l.idLicitacion = :idLicitacion")
    , @NamedQuery(name = "Licitacion.findByMontoPresupuestoAprobado", query = "SELECT l FROM Licitacion l WHERE l.montoPresupuestoAprobado = :montoPresupuestoAprobado")
    , @NamedQuery(name = "Licitacion.findByTipoContratacion", query = "SELECT l FROM Licitacion l WHERE l.tipoContratacion = :tipoContratacion")
    , @NamedQuery(name = "Licitacion.findByTipoLicitacion", query = "SELECT l FROM Licitacion l WHERE l.tipoLicitacion = :tipoLicitacion")})
public class Licitacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 20)
    @Column(name = "ANEXO_SOLICITUD_PRESUPUESTO")
    private String anexoSolicitudPresupuesto;
    @Size(max = 20)
    @Column(name = "CARACTER_LICITACION")
    private String caracterLicitacion;
    @Size(max = 20)
    @Column(name = "CLAVE_GANADOR")
    private String claveGanador;
    @Size(max = 20)
    @Column(name = "DESCRIPCION_CORTA")
    private String descripcionCorta;
    @Size(max = 40)
    @Column(name = "DESCRIPCION_DETALLADA")
    private String descripcionDetallada;
    @Size(max = 20)
    @Column(name = "DOCUMENTOS_ANEXO")
    private String documentosAnexo;
    @Size(max = 20)
    @Column(name = "EMPRESA_AGENTE_ASIGNADO")
    private String empresaAgenteAsignado;
    @Column(name = "FECHA_ADJUDICACION")
    @Temporal(TemporalType.DATE)
    private Date fechaAdjudicacion;
    @Column(name = "FECHA_APERTURA_SESION_PREGUNTAS")
    @Temporal(TemporalType.DATE)
    private Date fechaAperturaSesionPreguntas;
    @Column(name = "FECHA_CIERRE_RECEPCION_PROPUESTAS")
    @Temporal(TemporalType.DATE)
    private Date fechaCierreRecepcionPropuestas;
    @Column(name = "FECHA_CIERRE_SESION_PREGUNTAS")
    @Temporal(TemporalType.DATE)
    private Date fechaCierreSesionPreguntas;
    @Column(name = "FECHA_CIERRE_SESION_RESPUESTAS")
    @Temporal(TemporalType.DATE)
    private Date fechaCierreSesionRespuestas;
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Column(name = "FECHA_PUBLICACION")
    @Temporal(TemporalType.DATE)
    private Date fechaPublicacion;
    @Column(name = "FECHA_RECEPCION_PROPUESTAS")
    @Temporal(TemporalType.DATE)
    private Date fechaRecepcionPropuestas;
    @Column(name = "FECHA_SESION_RESPUESTAS")
    @Temporal(TemporalType.DATE)
    private Date fechaSesionRespuestas;
    @Column(name = "FOLIO_LICITACION")
    private Integer folioLicitacion;
    @Column(name = "FOLIO_PRESUPUESTO_APROBADO")
    private Integer folioPresupuestoAprobado;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_LICITACION")
    private Integer idLicitacion;
    @Column(name = "MONTO_PRESUPUESTO_APROBADO")
    private Integer montoPresupuestoAprobado;
    @Size(max = 20)
    @Column(name = "TIPO_CONTRATACION")
    private String tipoContratacion;
    @Size(max = 20)
    @Column(name = "TIPO_LICITACION")
    private String tipoLicitacion;

    public Licitacion() {
    }

    public Licitacion(Integer idLicitacion) {
        this.idLicitacion = idLicitacion;
    }

    public String getAnexoSolicitudPresupuesto() {
        return anexoSolicitudPresupuesto;
    }

    public void setAnexoSolicitudPresupuesto(String anexoSolicitudPresupuesto) {
        this.anexoSolicitudPresupuesto = anexoSolicitudPresupuesto;
    }

    public String getCaracterLicitacion() {
        return caracterLicitacion;
    }

    public void setCaracterLicitacion(String caracterLicitacion) {
        this.caracterLicitacion = caracterLicitacion;
    }

    public String getClaveGanador() {
        return claveGanador;
    }

    public void setClaveGanador(String claveGanador) {
        this.claveGanador = claveGanador;
    }

    public String getDescripcionCorta() {
        return descripcionCorta;
    }

    public void setDescripcionCorta(String descripcionCorta) {
        this.descripcionCorta = descripcionCorta;
    }

    public String getDescripcionDetallada() {
        return descripcionDetallada;
    }

    public void setDescripcionDetallada(String descripcionDetallada) {
        this.descripcionDetallada = descripcionDetallada;
    }

    public String getDocumentosAnexo() {
        return documentosAnexo;
    }

    public void setDocumentosAnexo(String documentosAnexo) {
        this.documentosAnexo = documentosAnexo;
    }

    public String getEmpresaAgenteAsignado() {
        return empresaAgenteAsignado;
    }

    public void setEmpresaAgenteAsignado(String empresaAgenteAsignado) {
        this.empresaAgenteAsignado = empresaAgenteAsignado;
    }

    public Date getFechaAdjudicacion() {
        return fechaAdjudicacion;
    }

    public void setFechaAdjudicacion(Date fechaAdjudicacion) {
        this.fechaAdjudicacion = fechaAdjudicacion;
    }

    public Date getFechaAperturaSesionPreguntas() {
        return fechaAperturaSesionPreguntas;
    }

    public void setFechaAperturaSesionPreguntas(Date fechaAperturaSesionPreguntas) {
        this.fechaAperturaSesionPreguntas = fechaAperturaSesionPreguntas;
    }

    public Date getFechaCierreRecepcionPropuestas() {
        return fechaCierreRecepcionPropuestas;
    }

    public void setFechaCierreRecepcionPropuestas(Date fechaCierreRecepcionPropuestas) {
        this.fechaCierreRecepcionPropuestas = fechaCierreRecepcionPropuestas;
    }

    public Date getFechaCierreSesionPreguntas() {
        return fechaCierreSesionPreguntas;
    }

    public void setFechaCierreSesionPreguntas(Date fechaCierreSesionPreguntas) {
        this.fechaCierreSesionPreguntas = fechaCierreSesionPreguntas;
    }

    public Date getFechaCierreSesionRespuestas() {
        return fechaCierreSesionRespuestas;
    }

    public void setFechaCierreSesionRespuestas(Date fechaCierreSesionRespuestas) {
        this.fechaCierreSesionRespuestas = fechaCierreSesionRespuestas;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(Date fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Date getFechaRecepcionPropuestas() {
        return fechaRecepcionPropuestas;
    }

    public void setFechaRecepcionPropuestas(Date fechaRecepcionPropuestas) {
        this.fechaRecepcionPropuestas = fechaRecepcionPropuestas;
    }

    public Date getFechaSesionRespuestas() {
        return fechaSesionRespuestas;
    }

    public void setFechaSesionRespuestas(Date fechaSesionRespuestas) {
        this.fechaSesionRespuestas = fechaSesionRespuestas;
    }

    public Integer getFolioLicitacion() {
        return folioLicitacion;
    }

    public void setFolioLicitacion(Integer folioLicitacion) {
        this.folioLicitacion = folioLicitacion;
    }

    public Integer getFolioPresupuestoAprobado() {
        return folioPresupuestoAprobado;
    }

    public void setFolioPresupuestoAprobado(Integer folioPresupuestoAprobado) {
        this.folioPresupuestoAprobado = folioPresupuestoAprobado;
    }

    public Integer getIdLicitacion() {
        return idLicitacion;
    }

    public void setIdLicitacion(Integer idLicitacion) {
        this.idLicitacion = idLicitacion;
    }

    public Integer getMontoPresupuestoAprobado() {
        return montoPresupuestoAprobado;
    }

    public void setMontoPresupuestoAprobado(Integer montoPresupuestoAprobado) {
        this.montoPresupuestoAprobado = montoPresupuestoAprobado;
    }

    public String getTipoContratacion() {
        return tipoContratacion;
    }

    public void setTipoContratacion(String tipoContratacion) {
        this.tipoContratacion = tipoContratacion;
    }

    public String getTipoLicitacion() {
        return tipoLicitacion;
    }

    public void setTipoLicitacion(String tipoLicitacion) {
        this.tipoLicitacion = tipoLicitacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLicitacion != null ? idLicitacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Licitacion)) {
            return false;
        }
        Licitacion other = (Licitacion) object;
        if ((this.idLicitacion == null && other.idLicitacion != null) || (this.idLicitacion != null && !this.idLicitacion.equals(other.idLicitacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.sai_agentecontratante.entidades.Licitacion[ idLicitacion=" + idLicitacion + " ]";
    }
    
}
