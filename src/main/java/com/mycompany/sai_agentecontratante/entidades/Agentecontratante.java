/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author darkm
 */
@Entity
@Table(name = "AGENTECONTRATANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agentecontratante.findAll", query = "SELECT a FROM Agentecontratante a")
    , @NamedQuery(name = "Agentecontratante.findByClaveagentecontratante", query = "SELECT a FROM Agentecontratante a WHERE a.claveagentecontratante = :claveagentecontratante")})
public class Agentecontratante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLAVEAGENTECONTRATANTE")
    private Integer claveagentecontratante;

    public Agentecontratante() {
    }

    public Agentecontratante(Integer claveagentecontratante) {
        this.claveagentecontratante = claveagentecontratante;
    }

    public Integer getClaveagentecontratante() {
        return claveagentecontratante;
    }

    public void setClaveagentecontratante(Integer claveagentecontratante) {
        this.claveagentecontratante = claveagentecontratante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (claveagentecontratante != null ? claveagentecontratante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agentecontratante)) {
            return false;
        }
        Agentecontratante other = (Agentecontratante) object;
        if ((this.claveagentecontratante == null && other.claveagentecontratante != null) || (this.claveagentecontratante != null && !this.claveagentecontratante.equals(other.claveagentecontratante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.sai_agentecontratante.entidades.Agentecontratante[ claveagentecontratante=" + claveagentecontratante + " ]";
    }
    
}
