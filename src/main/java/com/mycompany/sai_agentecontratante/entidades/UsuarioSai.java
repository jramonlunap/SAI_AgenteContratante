/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author darkm
 */
@Entity
@Table(name = "USUARIO_SAI")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioSai.findAll", query = "SELECT u FROM UsuarioSai u")
    , @NamedQuery(name = "UsuarioSai.findByContrase\u00f1a", query = "SELECT u FROM UsuarioSai u WHERE u.contrase\u00f1a = :contrase\u00f1a")
    , @NamedQuery(name = "UsuarioSai.findByIdUsuario", query = "SELECT u FROM UsuarioSai u WHERE u.idUsuario = :idUsuario")
    , @NamedQuery(name = "UsuarioSai.findByUsuario", query = "SELECT u FROM UsuarioSai u WHERE u.usuario = :usuario")})
public class UsuarioSai implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 20)
    @Column(name = "CONTRASE\u00d1A")
    private String contraseña;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USUARIO")
    private Integer idUsuario;
    @Size(max = 20)
    @Column(name = "USUARIO")
    private String usuario;

    public UsuarioSai() {
    }

    public UsuarioSai(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioSai)) {
            return false;
        }
        UsuarioSai other = (UsuarioSai) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.sai_agentecontratante.entidades.UsuarioSai[ idUsuario=" + idUsuario + " ]";
    }
    
}
