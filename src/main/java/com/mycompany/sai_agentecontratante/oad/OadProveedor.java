/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.oad;

import com.mycompany.sai_agentecontratante.entidades.Proveedor;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author darkm
 */
public interface OadProveedor extends JpaRepository<Proveedor, Integer>{

    @Override
    public List<Proveedor> findAll();
    
    void crear(Proveedor proveedor);

    void actualizar(Proveedor proveedor);

    List<Proveedor> buscarPorNombre(String nombre);

    Proveedor getPorId(Integer id);
}
