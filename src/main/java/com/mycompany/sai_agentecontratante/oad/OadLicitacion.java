/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.oad;

import com.mycompany.sai_agentecontratante.entidades.Licitacion;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author darkm
 */
public interface OadLicitacion  extends JpaRepository<Licitacion, Integer>{

    @Override
    public List<Licitacion> findAll();

    void crear(Licitacion licitacion);

    void actualizar(Licitacion licitacion);

    List<Licitacion> buscarPorNombre(String nombre);

    Licitacion getPorId(Integer id);
}
