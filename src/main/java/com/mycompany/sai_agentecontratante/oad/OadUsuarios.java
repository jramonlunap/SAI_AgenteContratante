/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.oad;

import com.mycompany.sai_agentecontratante.entidades.UsuarioSai;

/**
 *
 * @author darkm
 */
public interface OadUsuarios {
    UsuarioSai getPorId(Integer id);
}
