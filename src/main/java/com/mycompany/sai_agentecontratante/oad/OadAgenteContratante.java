/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.sai_agentecontratante.oad;

import com.mycompany.sai_agentecontratante.entidades.Agentecontratante;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author darkm
 */
public interface OadAgenteContratante {

    void crear(Agentecontratante agentecontratante);

    List<Agentecontratante> buscarPorNombre(String nombre);

    @Query("SELECT e FROM Agentecontratante e WHERE e.agentecontratante.claveagentecontratante = :claveagentecontratante")
    Agentecontratante getPorId(@Param("id") Integer idAgente);
}
