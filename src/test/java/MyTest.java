/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.sai_agentecontratante.entidades.Agentecontratante;
import com.mycompany.sai_agentecontratante.entidades.Licitacion;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author darkm
 */
public class MyTest {
    
    public MyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAgenteContratante() {
        System.out.println("id Agente Contratante");
        int idAgente = 1;
        Agentecontratante instance = new Agentecontratante();
        instance.setClaveagentecontratante(idAgente);
        int[] expResult = null;
        int result = instance.getClaveagentecontratante();
        System.out.println("id agente: " + result);
        System.out.println("--------------------------------\n");
    }
    
    @Test
    public void testLicitacion() {
        System.out.println("id Licitacion");
        String idLicitacion = "30DPA03";
        Licitacion instance = new Licitacion();
        instance.setClaveGanador(idLicitacion);
        int[] expResult = null;
        String result = instance.getClaveGanador();
        System.out.println("id Licitacion: " + result);
        System.out.println("--------------------------------\n");
    }
    
    @Test
    public void testDescripcion() {
        System.out.println("Descripcion ganador");
        String descripcion = "Esta es una descripcion de prueba";
        Licitacion instance = new Licitacion();
        instance.setDescripcionCorta(descripcion);
        int[] expResult = null;
        String result = instance.getDescripcionCorta();
        System.out.println("Descripcion ganador: " + result);
        System.out.println("--------------------------------\n");
    }
}
